use rand;
use num_bigint::{BigUint, BigInt, RandBigInt, ToBigInt};
use num_traits::*;
use num_integer::Integer;

fn gen_prime(bitsize: usize) -> BigUint {
    let mut rng = rand::thread_rng();
    let m = BigUint::one() << (bitsize - 1);
    loop {
        let mut p = rng.gen_biguint(bitsize);
        p |= BigUint::from(1u8);
        p |= m.clone();
        debug_assert_eq!(p.bits(), bitsize);
        if miller_rabin(&p) {
            return p;
        }
    }
}

fn miller_rabin(n: &BigUint) -> bool {
    const K: usize = 256;
    let mut rng = rand::thread_rng();
    let n_subone = n - BigUint::one();
    let (r, d): (BigUint, BigUint) = {
        let mut r = BigUint::one();
        let mut d = n_subone.clone();
        while d.is_even() {
            d = d / 2u8;
            r += 1u8;
        }
        (r / 2u8, d)
    };
    for _ in 0..K {
        let a = rng.gen_biguint_range(&BigUint::from(2u8), &n_subone);
        let mut x = a.modpow(&d, &n);
        if x == BigUint::one() || x == n_subone {
            continue;
        }
        let mut i = BigUint::zero();
        while i < r.clone() - BigUint::one() {
            x = x.pow(2u8) % 2u8;
            if x == n_subone {
                continue;
            }
            i += 1u8;
        }
        return false;
    }
    true
}

fn mmi(a: BigInt, n: BigInt) -> BigUint {
    let mut t = (BigInt::zero(), BigInt::one());
    let mut r = (n.clone(), a);
    while r.1 != BigInt::zero() {
        let q = r.0.clone() / r.1.clone();
        t = (t.1.clone(), t.0 - q.clone() * t.1);
        r = (r.1.clone(), r.0 - q * r.1);
    }
    if &r.0 > &BigInt::one() {
        panic!("a is not invertible");
    }
    if &t.0 < &BigInt::zero() {
        t.0 += n;
    }
    t.0.to_biguint().unwrap()
}

fn bytelen(bitlen: usize) -> usize {
    (bitlen / 8) + if (bitlen % 8) == 0 { 0 } else { 1 }
}

pub struct RsaKey {
    n: BigUint,
    ed: BigUint,
}

impl RsaKey {
    pub fn generate_keys(key_bitsize: usize) -> (RsaKey, RsaKey) {
        let bitsize = key_bitsize / 2;
        let mut rng = rand::thread_rng();
        let p = gen_prime(bitsize);
        let q = gen_prime(bitsize);
        let n = p.clone() * q.clone();
        let phi = (p - BigUint::one()) * (q - BigUint::one());
        let e = loop {
            let e = rng.gen_biguint_range(&BigUint::from(32000u32), &phi);
            if phi.gcd(&e) == BigUint::one() { break e; }
        };
        let private = RsaKey {
            n: n.clone(),
            ed: mmi(e.clone().to_bigint().unwrap(), phi.to_bigint().unwrap()),
        };
        let public = RsaKey {
            n: n,
            ed: e
        };
        (private, public)
    }

    pub fn encrypt(&self, bytes: &[u8]) -> Vec<u8> {
        let mut out = vec![];
        let bitlen = self.n.bits();
        let bytelen = bytelen(bitlen);
        let chunk_bytelen = (bitlen - 1) / 8;
        for chunk in bytes.chunks(chunk_bytelen) {
            let msg = BigUint::from_bytes_be(chunk);
            debug_assert!(&msg < &self.n);

            let encrypted = msg.modpow(&self.ed, &self.n);
            let mut c = encrypted.to_bytes_be();
            debug_assert!(c.len() <= bytelen);
            if c.len() < bytelen {
                c.insert(0, 0);
            }
            out.append(&mut c);
        }
        out
    }

    pub fn decrypt(&self, bytes: &[u8]) -> Vec<u8> {
        let mut out: Vec<u8> = Vec::new();
        let bitlen = self.n.bits();
        let bytelen = bytelen(bitlen);
        for chunk in bytes.chunks(bytelen) {
            let encrypted = BigUint::from_bytes_be(chunk);
            let msg = encrypted.modpow(&self.ed, &self.n);
            out.append(&mut msg.to_bytes_be());
        }
        out
    }

    pub fn from_file(path: &std::path::Path) -> std::io::Result<RsaKey> {
        use std::io::prelude::*;
        let mut file = std::fs::File::open(path)?;
        let mut contents = vec![];
        file.read_to_end(&mut contents)?;
        let mut it = contents.splitn(2, |byte| *byte as char == ' ');
        let (n, ed) = (it.next().unwrap(), it.next().unwrap());
        let (n, ed) = (std::str::from_utf8(n).unwrap(), std::str::from_utf8(ed).unwrap());
        Ok(RsaKey {
            // n: BigUint::from_bytes_be(n),
            // ed: BigUint::from_bytes_be(ed),
            // n: BigUint::from_radix_be(n, 2).unwrap(),
            // ed: BigUint::from_radix_be(ed, 2).unwrap(),
            n: BigUint::from_str_radix(n, 8).unwrap(),
            ed: BigUint::from_str_radix(ed, 8).unwrap(),
        })
    }

    pub fn to_file(&self, path: &std::path::Path) -> std::io::Result<()> {
        use std::io::Write;
        let mut file = std::fs::File::create(path)?;
        file.write_all(&self.n.to_str_radix(8).as_bytes())?;
        // file.write_all(&self.n.to_radix_be(2))?;
        file.write_all(" ".as_bytes())?;
        file.write_all(&self.ed.to_str_radix(8).as_bytes())?;
        // file.write_all(&self.ed.to_radix_be(2))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::RsaKey;
    #[test]
    fn it_works() {
        let bytes = [1u8, 2u8, 3u8, 4u8, 5u8, 6u8, 7u8, 8u8];
        let (private, public) = RsaKey::generate_keys(128);
        let encrypted = public.encrypt(&bytes);
        let decrypted = private.decrypt(&encrypted);
        assert_eq!(&bytes, decrypted.as_slice())
    }
}
