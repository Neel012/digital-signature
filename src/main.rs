#![windows_subsystem = "windows"]

mod rsa;
mod sign;

use relm::*;
use gtk;
use gio::prelude::*;
use gtk::prelude::*;
use relm_attributes::widget;

use std::path::PathBuf;

fn file_filter(pattern: &str, name: &str)-> gtk::FileFilter {
    let f = gtk::FileFilter::new();
    f.add_pattern(pattern);
    gtk::FileFilterExt::set_name(&f, Some(name));
    f
}

#[derive(relm_derive::Msg)]
pub enum Msg {
    FileToSign(Option<PathBuf>),
    KeyPrivate(Option<PathBuf>),
    Sign,
    FileToVerify(Option<PathBuf>),
    KeyPublic(Option<PathBuf>),
    Verify,
    GenerateKeys,
    Quit,
}

pub struct Model {
    key_private: Option<rsa::RsaKey>,
    key_public: Option<rsa::RsaKey>,
    file_to_sign: Option<PathBuf>,
    file_to_verify: Option<PathBuf>,
}

#[widget]
impl relm::Widget for Win {
    fn model() -> Model {
        let filter_private = gtk::FileFilter::new();
        filter_private.add_mime_type("pri");
        Model {
            key_private: None,
            key_public: None,
            file_to_sign: None,
            file_to_verify: None,
        }
    }

    fn handle_key(path: &Option<PathBuf>, model: &mut Option<rsa::RsaKey>, label: &gtk::Label) {
        match path {
            Some(path) => match rsa::RsaKey::from_file(&path) {
                Ok(k) => { *model = Some(k) },
                Err(e) => Self::signal(label, false, &e.to_string()),
            },
            None => Self::signal(label, false, "Missing private key"),
        }
    }

    fn update(&mut self, event: Msg) {
        use self::Msg::*;
        self.verify_label.hide();
        self.sign_label.hide();
        match event {
            FileToSign(path) => self.model.file_to_sign = path,
            FileToVerify(path) => self.model.file_to_verify = path,
            KeyPrivate(path)
                => Self::handle_key(&path, &mut self.model.key_private, &self.sign_label),
            KeyPublic(path)
                => Self::handle_key(&path, &mut self.model.key_public, &self.verify_label),
            Sign => match (&self.model.key_private, &self.model.file_to_sign) {
                (Some(priv_key), Some(path)) => match sign::sign(&priv_key, &path) {
                    Ok(_) => Self::signal(&self.sign_label, true, "Signature created"),
                    Err(e) => Self::signal(&self.sign_label, false, &e.to_string()),
                }
                (_, _) => Self::signal(&self.sign_label, false, "Select file for signing"),
            }
            Verify => match (&self.model.key_public, &self.model.file_to_verify) {
                (Some(pub_key), Some(path))
                    => match sign::verify(&pub_key, &path) {
                        Ok(true) => self.signal_verify(true, "File is authentic"),
                        Ok(false) => self.signal_verify(false, "File is not authentic"),
                        Err(e) => self.signal_verify(false, &e.to_string()),
                    },
                (_, _) => self.signal_verify(false, "Missing zip file"),
            },
            GenerateKeys => {
                let bits = usize::from_str_radix(&self.keys_bitsize.get_buffer().get_text(), 10).unwrap();
                let name = &self.keys_name.get_buffer().get_text();
                let path = PathBuf::new().with_file_name(name);
                let (private, public) = rsa::RsaKey::generate_keys(bits);
                private.to_file(&path.with_extension("pri")).unwrap();
                public.to_file(&path.with_extension("pub")).unwrap();
            }
            Quit => gtk::main_quit(),
        }
    }

    fn signal(label: &gtk::Label, ok: bool, text: &str) {
        label.show();
        let bg_color = if ok { gdk::RGBA::green() } else { gdk::RGBA::red() };
        label.override_background_color(gtk::StateFlags::empty(), Some(&bg_color));
        label.set_text(text);
    }

    fn signal_verify(&self, ok: bool, text: &str) {
        Self::signal(&self.verify_label, ok, text);
    }

    fn init_view(&mut self) {
        self.chooser_private.add_filter(&file_filter("*.pri", "Private Key"));
        self.chooser_private.add_filter(&file_filter("*", "All Files"));
        self.chooser_file_to_sign.add_filter(&file_filter("*.msg", "Messages"));
        self.chooser_file_to_sign.add_filter(&file_filter("*", "All Files"));
        self.chooser_public.add_filter(&file_filter("*.pub", "Public Key"));
        self.chooser_public.add_filter(&file_filter("*", "All Files"));
        self.chooser_file_to_verify.add_filter(&file_filter("*.zip", "Messages"));
        self.chooser_file_to_verify.add_filter(&file_filter("*", "All Files"));
    }

    view! {
        gtk::Window {
            title: "Digital Signature",
            property_default_height: 650,
            property_default_width: 800,
            delete_event(_, _) => (Msg::Quit, gtk::Inhibit(false)),
            gtk::Notebook {
                gtk::Grid {
                    valign: gtk::Align::Start,
                    halign: gtk::Align::Center,
                    margin_top: 18,
                    column_spacing: 24,
                    child: {
                        tab_label: Some("Create Signature"),
                    },
                    gtk::Label {
                        text: "Pick document for signing: ",
                        cell: {
                            left_attach: 0,
                            top_attach: 0,
                        },
                    },
                    gtk::Label {
                        text: "Load private key: ",
                        cell: {
                            left_attach: 1,
                            top_attach: 0,
                        },
                    },
                    #[name="chooser_file_to_sign"]
                    gtk::FileChooserButton {
                        file_set(chooser) => Msg::FileToSign(chooser.get_filename()),
                        cell: {
                            left_attach: 0,
                            top_attach: 1,
                        },
                    },
                    #[name="chooser_private"]
                    gtk::FileChooserButton {
                        title: "Load private key",
                        file_set(chooser) => Msg::KeyPrivate(chooser.get_filename()),
                        cell: {
                            left_attach: 1,
                            top_attach: 1,
                        },
                    },
                    gtk::Button {
                        label: "Create Signature",
                        clicked => Msg::Sign,
                        cell: {
                            left_attach: 2,
                            top_attach: 1,
                        },
                    },
                    #[name="sign_label"]
                    gtk::Label {
                        cell: {
                            left_attach: 1,
                            top_attach: 3,
                        },
                    },
                },
                gtk::Grid {
                    valign: gtk::Align::Start,
                    halign: gtk::Align::Center,
                    margin_top: 18,
                    column_spacing: 24,
                    child: {
                        tab_label: Some("Verify Signature"),
                    },
                    gtk::Label {
                        text: "Load zip file to verify: ",
                        cell: {
                            left_attach: 0,
                            top_attach: 0,
                        },
                    },
                    gtk::Label {
                        text: "Load public key: ",
                        cell: {
                            left_attach: 1,
                            top_attach: 0,
                        },
                    },
                    #[name="chooser_file_to_verify"]
                    gtk::FileChooserButton {
                        file_set(chooser) => Msg::FileToVerify(chooser.get_filename()),
                        cell: {
                            left_attach: 0,
                            top_attach: 1,
                        },
                    },
                    #[name="chooser_public"]
                    gtk::FileChooserButton {
                        file_set(chooser) => Msg::KeyPublic(chooser.get_filename()),
                        cell: {
                            left_attach: 1,
                            top_attach: 1,
                        },
                    },
                    gtk::Button {
                        label: "Verify Signature",
                        clicked => Msg::Verify,
                        cell: {
                            left_attach: 2,
                            top_attach: 1,
                        },
                    },
                    #[name="verify_label"]
                    gtk::Label {
                        cell: {
                            left_attach: 1,
                            top_attach: 3,
                        },
                    },
                },
                gtk::Grid {
                    valign: gtk::Align::Start,
                    halign: gtk::Align::Center,
                    margin_top: 18,
                    column_spacing: 24,
                    child: {
                        tab_label: Some("Generate Keys"),
                    },
                    gtk::Label {
                        text: "Key size in bits: ",
                        cell: {
                            left_attach: 0,
                            top_attach: 0,
                        },
                    },
                    gtk::Label {
                        text: "Key name: ",
                        cell: {
                            left_attach: 1,
                            top_attach: 0,
                        },
                    },
                    #[name="keys_bitsize"]
                    gtk::Entry {
                        cell: {
                            left_attach: 0,
                            top_attach: 1,
                        },
                    },
                    #[name="keys_name"]
                    gtk::Entry {
                        cell: {
                            left_attach: 1,
                            top_attach: 1,
                        },
                    },
                    gtk::Button {
                        label: "Generate key",
                        clicked => Msg::GenerateKeys,
                        cell: {
                            left_attach: 2,
                            top_attach: 1,
                        },
                    },
                },
            },
        }
    }
}

fn main() {
    // let appid = "org.gnome.DigitalSignature";
    // let application = gtk::Application::new(appid, gio::ApplicationFlags::empty()).unwrap();
    // application.set_property_resource_base_path(Some("/org/gnome/DigitalSignature"));
    Win::run(()).expect("Failed to setup GUI");
}
