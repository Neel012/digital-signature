use super::rsa;
use std::path::Path;
use std::fs::File;

pub fn sign(private_key: &rsa::RsaKey, path: &Path) -> std::io::Result<()> {
    use sha2::{Sha256, Digest};
    use std::io::prelude::*;
    let mut file = File::open(path)?;
    let mut contents: Vec<u8> = vec![];
    let mut hasher = Sha256::new();
    std::io::copy(&mut file, &mut hasher)?;
    file.seek(std::io::SeekFrom::Start(0))?;
    file.read_to_end(&mut contents)?;
    let hash = private_key.encrypt(hasher.result().as_slice());
    let output_path = path.with_extension("zip");
    let output = File::create(output_path)?;
    let tmp_hash_path = path.with_extension("hash");
    let hash_filename = tmp_hash_path.file_name().unwrap().to_str().unwrap();
    let filename = path.file_name().unwrap().to_str().unwrap();
    let mut zip = zip::ZipWriter::new(output);
    let opts = zip::write::FileOptions::default().compression_method(zip::CompressionMethod::Stored);
    zip.start_file(hash_filename, opts)?;
    zip.write(&hash)?;
    zip.start_file(filename, opts)?;
    zip.write(&contents)?;
    zip.finish()?;
    Ok(())
}

#[derive(Debug)]
pub enum SignError {
    ZipError(zip::result::ZipError),
    IoError(std::io::Error),
}

impl From<std::io::Error> for SignError {
    fn from(err: std::io::Error) -> Self {
        SignError::IoError(err)
    }
}

impl From<zip::result::ZipError> for SignError {
    fn from(err: zip::result::ZipError) -> Self {
        SignError::ZipError(err)
    }
}

impl std::fmt::Display for SignError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            SignError::IoError(ref err) => write!(f, "IO error: {}", err),
            SignError::ZipError(ref err) => write!(f, "Zip error: {}", err),
        }
    }
}

pub fn verify(public_key: &rsa::RsaKey, path: &Path) -> Result<bool, SignError> {
    use std::io::Read;
    use sha2::{Sha256, Digest};
    let file = File::open(path)?;
    let msg_path = path.with_extension("msg");
    let mut zip = zip::ZipArchive::new(file)?;
    let hash = {
        let hash_path = path.with_extension("hash");
        let mut hash_zip = zip.by_name(hash_path.file_name().unwrap().to_str().unwrap())?;
        let mut hash = vec![];
        hash_zip.read_to_end(&mut hash)?;
        public_key.decrypt(&hash)
    };
    let mut msg = zip.by_name(msg_path.file_name().unwrap().to_str().unwrap())?;
    let mut hasher = Sha256::new();
    std::io::copy(&mut msg, &mut hasher)?;
    Ok(&hash == &hasher.result().to_vec())
}

